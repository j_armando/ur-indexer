from bottle import run, route, view, static_file, get, post, response, request, default_app
import os
import json
from mongoengine import *
from bson.objectid import *
from mongoengine.base import BaseList
from mongoModels import *
from os import listdir
from os.path import isfile, join

env = os.getenv('PYTHONSTARTUP', 'localhost')
#ds063287.mongolab.com:63287/mobiusdb 
connect("mobiusdb", host="ds063287.mongolab.com", port=63287, username="remote", password="pass")

#code for getting the guns (js)
@route('/scripts/<fileName>')
def sendJS(fileName):
    return static_file(fileName, root='scripts')

@route('/shared/js/<fileName>')
def requireJs(fileName):
    return static_file(fileName, root="scripts/shared")

@route('/app/<filepath:path>')
def getAngularApp(filepath):
    return static_file(filepath, root="angular")

@route('/content/<filepath:path>')
def sendPdf(filepath):
	return static_file(filepath, root="content")

@route('/')
def index():
    return static_file("index.html", root="")

#Restful API
@get('/file')
def getPdf():
	mypath = os.path.dirname(__file__) + '/content/files'
	onlyfiles = [ f for f in listdir(mypath) if isfile(join(mypath,f)) ]
	files = list()
	for v in onlyfiles:
		fileInfo = {'fileName': v}
		files.append(fileInfo)
		print files
	response.content_type = 'application/json'
	print  files
	return json.dumps(files)

@get('/indexedFile')
def getIndexedFile():
	response.content_type = 'application/json'
	indexed = [convertMongoToDict(f) for f in IndexedFile.objects]
	return json.dumps(indexed)

@post('/indexedFile')
def addIndexedFile():
    message = {'bla':'ble'}
    try:
        indexedFile = IndexedFile()
        for k,v in request.json.items(): #need to validate the json
            setattr(indexedFile, k, v)
            print k
        indexedFile.save()
        
        message = {'message':'Indexed file information successfully saved'}
    
    except Exception as e:
       message = {'error':str(e)}

    return message

#internal methods
def convertMongoToDict(doc): #This method converts any MongoEngine document into a python dict
    result = doc.to_mongo()
    result['_id'] = str(result['_id'])
    #print str(doc._id)

    for k, v in doc._fields.iteritems():
        if k.endswith('_ObjectId'): #For normalized relationship (relational DBs) an ObjectId is used on the embeded document, need to parse it
            result[k] = str(result[k])
        elif isinstance(doc[k], datetime.datetime): # Need to strip the time from datetime fields to be json serializable
            result[k] = str(datetime.date(doc[k].year, doc[k].month, doc[k].day))
            print result[k]
        else:    
            if k != 'id' and k != '_id':
                if isinstance(doc[k], BaseList): #looking for lists inside the element 'k'
                    childCollection = list()
                    for a in doc[k]:
                        if '_id' not in a: # this part validates if the element is not a mongoengine object
                        #for this part to work correctly the embedded documents need to have an ObjectID
                            childCollection.append(a)
                        else: # if it IS a mongoengine object, then it needs to be converted
                            childCollection.append(convertMongoToDict(a)) 
                    doc[k] = childCollection
                result[k] = doc[k]
                
    return result



run(host='localhost', port=8080)