require([
			'app'
			,'/app/controllers/pdfController.js'
			,'/app/controllers/indexedFilesController.js'
			//bellow here are the shared js resources
			,'/shared/js/angular-resource.min.js'
			,'/shared/js/angular-route.min.js'
			,'/shared/js/angular-animate.min.js'
			,'/shared/js/jquery-2.1.1.min.js'
			,'/shared/js/bootstrap.min.js'
			/*
			,'/shared/js/pdf.compat.js'
			,'/shared/js/ng-pdfviewer.js'
			,'/shared/js/pdf.js'
			,'/shared/js/pdf.worker.js'
			*/
			,'/shared/js/ui-bootstrap-tpls-0.11.0.min.js'
		], 
		function() {
//			var container = document.getElementById('main-container');
//			container.setAttribute('ng-controller', 'SomeLazyController');
			angular.bootstrap(document, ['indexerApp']);
		}
	);
