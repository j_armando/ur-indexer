var app = angular.module('indexerApp', [ 'ngPDFViewer', 'ui.bootstrap', 'ngResource', 'ngRoute', 'ngAnimate']);

app.config(['$routeProvider', '$locationProvider', 
	function($routeProvider, $locationProvider){

		$routeProvider
			.when('/index-pdfs', {
				templateUrl: '/app/views/pdf-non-indexed.html',
				controller:'pdfCtrl'
			})
			.when('/search-documents', {
				templateUrl:'/app/views/search-indexed-pdf.html',
				controller:'indexedFilesCtrl'
			});
}]);


app.service('Files', function($resource){
	return $resource(
		"/file/:Id",
		{Id:"@Id"}
		);
});

app.service('IndexedFiles', function($resource){
	return $resource(
		"/indexedFile/:Id",
		{Id:"@Id"}
		);
});

console.log('batida');
