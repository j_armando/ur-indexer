#models
from mongoengine import *
import datetime

class User(Document):
	_id = ObjectIdField()
	name = StringField(required=True)
	lastName = StringField()
	meta = {'collection':'users'}

class IndexedFile(Document):
	_id = ObjectIdField()
	fileName = StringField()
	category = StringField()
	creationDate = DateTimeField(default=datetime.datetime.now)
	registrationId = StringField()
	currentPath = StringField()
	meta = {'collection':'IndexedFiles'}

class Client(Document):
	_id = ObjectIdField()
	name = StringField(required=True)
	lastName = StringField()
	cedula = StringField()
	email = StringField()
	phoneNumbers = StringField()
	address = StringField()
	notes = StringField()
	creationDate = DateTimeField()
	modificationDate = DateTimeField()
	meta = {'collection':'clients'}


