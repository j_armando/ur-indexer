app.controller('pdfCtrl', [ '$scope', 'PDFViewerService', 'Files', function($scope, pdf, Files) {
    $scope.viewer = pdf.Instance("viewer");
    $scope.files = Files.query();
    $scope.currentPdf = "/content/files/blah.pdf";
    path = "/content/files/";
    
    $scope.changePdf = function(file){
        $scope.fileName = file.fileName;
        $scope.currentPdf = path + file.fileName;
    };

    $scope.nextPage = function() {
        $scope.viewer.nextPage();
    };

    $scope.prevPage = function() {
        $scope.viewer.prevPage();
    };

    $scope.pageLoaded = function(curPage, totalPages) {
        $scope.currentPage = curPage;
        $scope.totalPages = totalPages;
    };

/*    $scope.totalPages = 2;
    $scope.totalItems = $scope.totalPages;
    $scope.itemsPerPage = 0;
*/
    $scope.setPage = function (pageNo) {
        $scope.vainita = pageNo;
    };

    $scope.pageChanged = function() {
        console.log('Page changed to: ' + $scope.vainita);
    };
}]);



