app.controller('indexedFilesCtrl', [ '$scope', 'IndexedFiles', function($scope, IndexedFiles){
    $scope.indexedFiles = IndexedFiles.query();
    $scope.selectedCategory = "Select a category";
    $scope.categories = ["fruit", "ninja", "car"];
    $scope.fileName = "";

    $scope.selectCategory = function(category){
        $scope.selectedCategory = category;
    };

    $scope.saveFileInfo = function(){
        $scope.indexedFile = new IndexedFiles();
        $scope.indexedFile.category = $scope.selectedCategory;
        $scope.indexedFile.fileName = $scope.fileName;
        $scope.indexedFile.$save();
    };
}]);
